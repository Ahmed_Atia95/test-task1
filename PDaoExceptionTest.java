package code;

import section.Patient;
import section.PatientDaoException;
import section.PatientDaoImpl;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import static org.junit.Assert.*;

public class PDaoExceptionTest {

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Test
    public void ConstructorTest() throws PatientDaoException{
        thrown.expect(PatientDaoException.class);
        //you can test the exception message like
        thrown.expectMessage("Patient Dao Exception");
        PatientDaoImpl dao = new PatientDaoImpl();

    }


}
